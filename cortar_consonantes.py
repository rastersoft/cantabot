#!/usr/bin/env python3

import matplotlib.pyplot as plt
import scipy.io.wavfile
import scipy.signal
import numpy as np
import sys
import math
import os

# leemos el fichero WAV de la consonante más la letra A
fichero_entrada = os.path.join('sonidos_alba', f'{sys.argv[1]}a.wav')

freq_muestreo, datosaudio = scipy.io.wavfile.read(fichero_entrada)
if (type(datosaudio[0]) != np.int16):
    # convertimos a MONO
    datosaudio = datosaudio.sum(axis=1) / 2
    datosaudio = datosaudio.astype(np.int16)

# leemos el fichero WAV de la vocal I para pegarlo al final
# Usamos una que sea diferente de la que ya tenía la consonante

freq_muestreo_vocal, datosaudio_vocal = scipy.io.wavfile.read('sonidos_alba/i_1seg.wav')
# no convertimos a mono porque ya sabemos que lo es

plt.ion()
plt.plot(datosaudio)
plt.show()

m1 = input("Muestra inicial: ")
m2 = input("Muestra final: ")

freq_fundamental = freq_muestreo / ((int(m2) - int(m1)))
print(f'Frecuencia: {freq_fundamental}')

plt.close()

# ajustamos la frecuencia

frecuencia_deseada = 150

tamano_final = int(len(datosaudio) * (freq_fundamental / frecuencia_deseada))

datosaudio = scipy.signal.resample(datosaudio, tamano_final)
datosaudio = datosaudio.astype(np.int16)

plt.plot(datosaudio)
plt.show()

corte = None
while True:

    m1 = 0
    m2 = input("Muestra de corte final: ")
    if m2 == "":
        break

    corte1 = int(m1)
    try:
        corte2 = int(m2)
        final = np.append(datosaudio[corte1:corte2], (datosaudio_vocal))
        nombre_fichero = 'test_sonido.wav'
        scipy.io.wavfile.write(nombre_fichero, freq_muestreo, final)
    except:
        pass
    os.system('mpv test_sonido.wav')

nombre_fichero = os.path.join('sonidos_finales', f'{sys.argv[1]}.wav')
scipy.io.wavfile.write(nombre_fichero, freq_muestreo, datosaudio[corte1:corte2])
