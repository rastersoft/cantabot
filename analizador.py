#!/usr/bin/env python3

import matplotlib.pyplot as plt
import scipy.io.wavfile
import scipy.signal
import numpy as np

import sys
import math
import os

# frecuencia final deseada
frecuencia_deseada = 150


# leemos el fichero WAV
if sys.argv[1] in ['a','e','i','o','u']:
    extra = ''
else:
    extra = 'a'
fichero_entrada = os.path.join('sonidos_alba', f'{sys.argv[1]}{extra}.wav')
freq_muestreo, datosaudio = scipy.io.wavfile.read(fichero_entrada)
if (type(datosaudio[0]) != np.int16):
    # pass to MONO
    datosaudio = datosaudio.sum(axis=1) / 2
    datosaudio = datosaudio.astype(np.int16)

plt.ion()
plt.plot(datosaudio)
plt.show()

m1 = int(input("Muestra inicial: "))
m2 = int(input("Muestra final: "))

plt.ioff()
plt.close()

datosaudio2 = datosaudio[m1:m2]

cuarta1 = int(m1 + ((m2-m1)/4))
cuarta2 = int(m1 + ((m2-m1)*3/4))

p1 = int(np.argmax(datosaudio[m1:cuarta1]))
p2 = int(np.argmax(datosaudio[cuarta2:m2]))

f1 = m1+p1
f2 = cuarta2+p2

datosaudio = datosaudio[f1:f2]

freq_fundamental = freq_muestreo / ((int(f2) - int(f1)))
print(f"Puntos de corte: {f1}:{f2}")
print(f'Frecuencia: {freq_fundamental}')

plt.plot(datosaudio)
plt.show()


# ajustamos la frecuencia

tamano_final = int(len(datosaudio) * (freq_fundamental / frecuencia_deseada))

datosaudio = scipy.signal.resample(datosaudio, tamano_final)
datosaudio = datosaudio.astype(np.int16)

fichero_salida = os.path.join('sonidos_finales', f'{sys.argv[1]}.wav')
scipy.io.wavfile.write(fichero_salida, freq_muestreo, datosaudio)
