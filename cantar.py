#!/usr/bin/env python3

import matplotlib.pyplot as plt
import scipy.io.wavfile
import numpy as np
import sys
import math
import scipy.signal
import os

class sintetizador(object):

    def __init__(self):
        # duración de las vocales que no se estiran para completar la nota
        self._duracion_vocal = 0.15
        # duración de las consonantes sonoras
        self._duracion_sonoras = 0.20

        self._muestreo = 44100

        self._frecuencia_base = 150 # frecuencia base de las consonantes
        self.salida = np.array([])

        self._sonidos = {}
        self._vocales = ['a','e','i','o','u']
        self._sonoras = ['b','m','n']
        self._resto = ['ch','d','f','g','h','j','k','l','ñ','p','r','rr','s','t','w','x','y','z']
        self._estrofa = 1

        for letra in self._vocales:
            self._almacena_sonido(letra)
        for letra in self._sonoras:
            self._almacena_sonido(letra)
        for letra in self._resto:
            self._almacena_sonido(letra)


    def _almacena_sonido(self, letra):
        muestreo, datos = scipy.io.wavfile.read(os.path.join('sonidos_finales', f'{letra}.wav'))
        if letra == 'ch':
            letra = 'c'
        elif letra == 'rr':
            letra = 'R'
        self._sonidos[letra] = datos


    def _repite_sonido(self, letra, duracion, nota):
        tamano = len(self._sonidos[letra])
        n_muestras = int(self._muestreo * duracion)

        # cogemos el doble de muestras para tener margen para recortar
        repeticion = n_muestras / tamano
        sonido = self._ajusta_a_nota(np.tile(self._sonidos[letra], math.ceil(repeticion * 2)), nota)
        # cortamos al tamaño deseado. Como repetimos el sample el doble de lo necesario,
        # y nunca subimos más de una octava, eso significa que lo que tenemos tras
        # ajustar una nota siempre será más largo que lo necesario, y por tanto,
        # al cortar aquí, nos quedará siempre en n_muestras exactamente, nunca menos.
        datos = sonido[:n_muestras]
        return datos

    def _render(self, texto, nota, muestras = None):
        """ Renderiza uno o varios fonemas """

        datos = np.array([])
        if len(texto) == 0:
            return datos
        if muestras is None:
            for letra in texto:
                if letra in self._vocales:
                    # es una vocal, así que repetimos lo suficiente como para que dure self._duracion_vocal
                    sonido = self._repite_sonido(letra, self._duracion_vocal, nota)
                elif letra in self._sonoras:
                    sonido = self._repite_sonido(letra, self._duracion_sonoras, nota)
                else:
                    sonido = self._ajusta_a_nota(self._sonidos[letra], nota)
                datos = np.append(datos, sonido)
        else:
            if muestras <= 0:
                print(f"Duracion no valida: {self._estrofa}, {texto}, {nota}, {muestras}")
            else:
                repeticiones = muestras / 294 # 294 muestras es el tamaño de todas las vocales; y las vocales son las únicas con duración
                datos = np.tile(self._sonidos[texto], math.ceil(repeticiones * 2)) # cogemos el doble de repeticiones para tener margen al recortar
                datos = self._ajusta_a_nota(datos, nota)
                datos = datos[:muestras] # nos quedamos con el número de muestras que necesitamos
        return datos

    def _factor_de_escala(self, nota):
        """ Devuelve el factor de escala necesario para convertir una frecuencia
            en SOL al semitono deseado """

        nota -= 5 # el SOL estará en 150Hz; o sea, será "tal cual"
        if nota != 0: # resampleamos para cambiar el tono
            if nota > 0:
                # 1.059463094 = raíz doceava de dos
                escala = 1/math.pow(1.059463094, nota)
            else:
                escala = math.pow(1.059463094, -nota)
        else:
            escala = 1
        return escala

    def _ajusta_a_nota(self, audiodata, nota):
        """ Ajusta la frecuencia de una muestra a la nota deseada """

        if nota != 5: # el SOL es 5 y estará en 150Hz; o sea, será "tal cual"
            duracion = len(audiodata) * self._factor_de_escala(nota)
            audiodata = scipy.signal.resample(audiodata, int(duracion))
        return audiodata


    def interpreta(self, nota, duracion, texto):
        """ La nota es un valor entero, que indica un semitono (ergo: 12 semitonos
            en una octava). El valor 5 es SOL, con lo que 4=FA, 2=MI, 0=RE, 7=LA, 9=SI, 10=DO mayor, 12=RE mayor,
            cubriendo el rango necesario para Daisy Bell. Además, 5 estará fijo en 150 Hz, que es la frecuencia
            "base" de las muestras. Eso significa que los semitonos por debajo de 5 implicarán "estirar" la
            muestra, y los que están por encima deben "comprimirla".
        """

        # separamos en consonantes finales, última vocal, y resto
        # la última vocal se "estirará" todo lo necesario para cubrir la duración
        # de la nota; las consonantes se reproducirán a su duración normal, y el
        # resto de vocales durarán tanto como indique self._duracion_vocal

        final = ""
        vocal = ""
        inicio = ""
        for index in range(len(texto)-1, -1, -1):
            letra = texto[index]
            if letra in self._vocales:
                vocal = letra
                inicio = texto[0:index]
                final = texto[index+1:]
                break
        b1 = self._render(inicio, nota)
        b3 = self._render(final, nota)
        muestras_totales = round(self._muestreo * duracion)
        muestras_b2 = muestras_totales - (len(b1) + len(b3))
        b2 = self._render(vocal, nota, muestras_b2)
        self.salida = np.append(self.salida, b1)
        self.salida = np.append(self.salida, b2)
        self.salida = np.append(self.salida, b3)
        self._estrofa += 1

    def pausa(self, duracion):
        """ Añade una pausa o silencio """
        pausa = [0] * round(self._muestreo * duracion)
        self.salida = np.append(self.salida, pausa)

    def grabar(self, nombre):
        """ Graba en formato WAV la canción """
        scipy.io.wavfile.write(nombre, self._muestreo, self.salida.astype(np.int16))
        self.salida = np.array([])
        self._estrofa = 1


# la redonda durará un segundo y medio
# el resto de duraciones dependerá de la primera
redonda = 1.3
redondadot = redonda * 1.5
blanca = redonda/2
blancadot = blanca * 1.5
negra = blanca/2
negradot = negra * 1.5

# correspondencia entre semitonos y notas
# los valores intermedios son "sostenido"/"bemol"
# pero no los pongo porque no los uso
nota_re = 0
nota_mi = 2
nota_fa = 4
nota_sol = 5
nota_la = 7
nota_si = 9
nota_do = 10
nota_re_mayor = 12

s = sintetizador()

# Canción "Daisy Bell"
s.interpreta(nota_re_mayor, blancadot, "dei")
s.interpreta(nota_si, blancadot, "si")
s.interpreta(nota_sol, blancadot, "dei")
s.interpreta(nota_re, blancadot, "si")

#5
s.interpreta(nota_mi, negra, "gif")
s.interpreta(nota_fa, negra, "mi")
s.interpreta(nota_sol, negra, "yuR")

#8
s.interpreta(nota_mi, negra, "an")
s.interpreta(nota_sol, negra, "seR")
s.interpreta(nota_re, blancadot+blanca, "du")

s.pausa(negra)

#11
s.interpreta(nota_la, blancadot, "aim")
s.interpreta(nota_re_mayor, blancadot, "jalf")
s.interpreta(nota_si, blancadot, "cRei")
s.interpreta(nota_sol, blancadot, "si")

#15
s.interpreta(nota_mi, negra, "ol")
s.interpreta(nota_fa, negra, "foR")
s.interpreta(nota_sol, negra, "de")
s.interpreta(nota_la, blanca, "lob")

#19
s.interpreta(nota_si, negra, "of")
s.interpreta(nota_la, blancadot+negra, "iu")

s.pausa(negra)

#21
s.interpreta(nota_si, negra, "it")
s.interpreta(nota_do, negra, "gunt")
s.interpreta(nota_si, negra, "bi")
s.interpreta(nota_la, negra, "a")
s.interpreta(nota_re_mayor, blanca, "stai")
s.interpreta(nota_si, negra, "lis")
s.interpreta(nota_la, negra, "ma")
s.interpreta(nota_sol, redonda, "Ris")

#29
s.interpreta(nota_la, negra, "ai")
s.interpreta(nota_si, blanca, "kant")
s.interpreta(nota_sol, negra, "af")
s.interpreta(nota_mi, blanca, "foRd")

#33
s.interpreta(nota_sol, negra, "a")
s.interpreta(nota_mi, negra, "ka")
s.interpreta(nota_re, blanca+negra, "Ric")

s.pausa(negra)

#36
s.interpreta(nota_re, negra, "bat")
s.interpreta(nota_sol, blanca, "yul")
s.interpreta(nota_si, negra, "lok")
s.interpreta(nota_la, blanca, "suit")

s.pausa(negra)

#40
s.interpreta(nota_sol, blanca, "on")
s.interpreta(nota_si, negra, "de")
s.interpreta(nota_la, negra, "sit")
s.interpreta(nota_si, negra, "of")
s.interpreta(nota_do, negra, "a")
s.interpreta(nota_re_mayor, negra, "bai")
s.interpreta(nota_si, negra, "si")
s.interpreta(nota_sol, negra, "col")

s.interpreta(nota_la, blanca, "built")
s.interpreta(nota_re, negra, "for")
s.interpreta(nota_sol, blancadot+blanca, "tu")

s.grabar('canta.wav')
