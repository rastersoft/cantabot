#!/usr/bin/env python3

import matplotlib.pyplot as plt
import scipy.io.wavfile
import numpy as np
import sys
import math
import scipy.signal
import os

class sintetizador(object):

    def __init__(self):
        self._duracion_vocal = 0.15
        self._duracion_sonoras = 0.15
        self._muestreo = 44100
        self._frecuencia_base = 150
        self._frecuencia_acento = 160

        self._pausas = {
            ' ': 2000,
            ',': 7000,
            ';': 7000,
            '.': 12000
        }
        self._sonidos = {}
        self._vocales = ['a', 'e', 'i', 'o', 'u', 'á', 'é', 'í', 'ó', 'ú']
        self._tildes = {
            'á': 'a',
            'é': 'e',
            'í': 'i',
            'ó': 'o',
            'ú': 'u'
        }
        self._sustituciones = {
            'y ': 'i ',
            'll': 'y',
            'ca': 'ka',
            'cá': 'ká',
            'ce': 'ze',
            'cé': 'zé',
            'ci': 'zi',
            'cí': 'zí',
            'co': 'ko',
            'có': 'kó',
            'cu': 'ku',
            'cú': 'kú',
            'qu': 'k',
            'ge': 'je',
            'gi': 'ji',
            'gé': 'jé',
            'gí': 'jí',
            'gué': 'gé',
            'guí': 'gí',
            'ch': 'c',
            'h': '',
            'rr': 'R',
            'v': 'b'
        }

        self._vocales2 = ['a','e','i','o','u']
        self._sonoras = ['b','m','n']
        self._resto = ['ch','d','f','g','h','j','k','l','ñ','p','r','rr','s','t','w','x','y','z']

        for letra in self._vocales2:
            self._almacena_sonido(letra)
        for letra in self._sonoras:
            self._almacena_sonido(letra)
        for letra in self._resto:
            self._almacena_sonido(letra)

    def _almacena_sonido(self, letra):
        muestreo, datos = scipy.io.wavfile.read(os.path.join('sonidos_finales', f'{letra}.wav'))
        if letra == 'ch':
            letra = 'c'
        elif letra == 'rr':
            letra = 'R'
        self._sonidos[letra] = datos


    def _repite_sonido(self, letra, duracion, frecuencia):
        tamano_deseado = int(self._muestreo / frecuencia)
        sonido = self._sonidos[letra]
        tamano = len(sonido)
        if tamano != tamano_deseado:
            sonido = scipy.signal.resample(sonido, tamano_deseado)
            tamano = tamano_deseado
        n_muestras = self._muestreo * duracion
        # cogemos el doble de muestras para tener margen para recortar
        repeticion = math.ceil(n_muestras / tamano)
        sonido = np.tile(sonido, int(repeticion))
        datos = sonido[:int(n_muestras)]
        return datos


    def hablar(self, texto):

        texto = texto.lower()
        self.salida = np.array([])
        for sustitucion in self._sustituciones:
            texto = texto.replace(sustitucion, self._sustituciones[sustitucion])

        # separamos adecuadamente las palabras
        texto = texto.replace(",", ", ")
        texto = texto.replace(".", ". ")
        texto = texto.replace(";", "; ")
        while texto.find("  ") != -1:
            texto = texto.replace("  ", " ")
        texto = texto.strip()

        for letra in texto:
            if letra in [' ', ',', ';', '.']:
                self._pausa(letra)
            elif letra in self._vocales:
                self._vocal(letra)
            elif letra in self._sonoras:
                self._sonora(letra)
            elif letra in self._resto:
                self._consonante(letra)
            else:
                print(f"Sonido desconocido para '{letra}'")

    def grabar(self, nombre):
        scipy.io.wavfile.write(nombre, 44100, self.salida.astype(np.int16))

    def _vocal(self, letra):
        if letra in self._tildes:
            frecuencia = self._frecuencia_acento
            letra = self._tildes[letra]
        else:
            frecuencia = self._frecuencia_base
        sonido = self._repite_sonido(letra, self._duracion_vocal, frecuencia)
        self.salida = np.append(self.salida, sonido)

    def _sonora(self, letra):
        datos = self._repite_sonido(letra, self._duracion_sonoras, self._frecuencia_base)
        self.salida = np.append(self.salida, datos)

    def _consonante(self, letra):
        self.salida = np.append(self.salida, self._sonidos[letra])

    def _pausa(self, letra):
        pausa = [0] * self._pausas[letra]
        self.salida = np.append(self.salida, pausa)

s = sintetizador()
texto = ""
for entrada in sys.argv[1:]:
    texto += " " + entrada
texto = texto[1:] # nos saltamos el primer espacio
s.hablar(texto)
s.grabar('habla.wav')
