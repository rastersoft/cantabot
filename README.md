# Cantabot

Cantabot es una versión de Alvabot (https://5ro4.itch.io/alvabot), un sintetizador
de voz creado por Alva Majo utilizando muestras de su propia voz.

Cantabot parte de dichas muestras y es capaz de entonar y de fijar una duración,
lo que permite hacer que *cante*.

El código de demostración genera un fichero WAV con la canción "Daisy Bell", una
melodía clásica en la síntesis de voz:

* En 1961 fue interpretada por primera vez por un ordenador: un IBM 7094
* En 2001, la unidad HAL 9000 de la astronave Discovery la cantó mientras sus
  centros superiores eran desconectados por el astronauta David Bowman
* En 3003, la unidad de doblado Bender Bending Rodriguez la interpretó para la
  nave de Planet Express, durante el corto periodo que estuvo enamorado de ella

## Estructura del proyecto

El directorio *sonidos_alba* contiene las muestras originales, así como diez
muestras reprocesadas de las vocales.

El directorio *sonidos finales* contiene las muestras vocales y consonantes ya
reprocesadas, tras ajustar todas ellas a una frecuencia fundamental de 150Hz y,
en el caso de las vocales, reducirlas a un único periodo para permitir
concatenarlas múltiples veces y así alargar su duración todo lo necesario para
poder cantar.

A mayores hay cuatro programas en python:

* analizador.py: permite cargar un fichero WAV y traza sus muestras en una gráfica
  ampliable, donde se pueden buscar las dos muestras que delimiten un periodo.
  Hecho ésto, el programa recortará dicho trozo y, además, mostrará en pantalla
  y ajustará su frecuencia fundamental.

* cortar_consonantes.py: lee un fichero con una consonante más la vocal A, y permite
  en primer lugar especificar dos muestras de dicha vocal (igual que en *analizador.py*)
  para determinar la frecuencia fundamental. Tras conocer dicha frecuencia,
  resampleará la muestra completa para ajustarla a 150Hz, y pedirá un nuevo valor
  de una muestra, que debería especificar el punto en el que acaba la consonante y
  empieza la vocal. Cortará el *sample* en ese punto, le añadirá a continuación
  un segundo de la vocal I, y lo reproducirá, lo que permite saber si el punto de
  corte es correcto. Si lo es, grabará el sample en *sonidos_finales*.

* hablar.py: es una recreación del Alvabot original: recibe una frase en la línea
  de comandos y la reproduce. Convierte el castellano a fonemas, y entiende puntos,
  comas y punto y comas. Además, pronuncia las vocales con tilde con un tono
  ligeramente superior, lo que permite añadir entonación (aunque aún no es capaz
  de hacerlo automáticamente). Utiliza las muestras generadas con los tres programas
  anteriores.

* cantar.py: es el programa que permite cantar. Se trata de un objeto en python
  con varios métodos que permiten ir añadiendo notas con los fonemas que se deben
  cantar en ella y su duración. Siempre debe haber al menos una vocal en cada nota.
  Se asume, además, que si hay varias vocales, sólo la última se estirará para
  completar la duración deseada de la nota; el resto de vocales y las consonantes
  tendrán una duración fija.

Los programas no reproducen el sonido en sí, sino que generan un fichero .WAV con
la salida, el cual debe reproducirse con un programa diferente.

## Licencia

Al igual que el código original de Alvabot, puedes "hacer lo que quieras" con éste.
Para dar más garantía legal, se distribuye bajo una licencia Expat (también conocida
como "licencia MIT"), que básicamente dice que puedes hacer lo que te de la gana.

## Autor

Cantabot ha sido creado por Raster Software, basado en Alvabot, original de
Alva Majo.

Sergio Costas (Raster Software Vigo)  
rastersoft@gmail.com  
https://www.rastersoft.com
